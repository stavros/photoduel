from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import Contest, Photo, User, Vote

admin.site.register(User, UserAdmin)


@admin.register(Contest)
class ContestAdmin(admin.ModelAdmin):
    list_display = ["name", "creator", "participation", "voting", "visibility", "stage"]
    search_fields = ["name"]
    list_filter = ["participation", "voting", "visibility", "stage"]


@admin.register(Photo)
class PhotoAdmin(admin.ModelAdmin):
    list_display = ["creator", "contest", "url"]


@admin.register(Vote)
class VoteAdmin(admin.ModelAdmin):
    list_display = ["user", "contest", "winner", "loser"]
