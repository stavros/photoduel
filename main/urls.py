"""The main application's URLs."""

from django.urls import path

from . import views

app_name = "main"
urlpatterns = [
    path('', views.index, name="index"),
    path('login/', views.login, name="login"),
    path('contest/<contest_id>/', views.contest, name="contest"),
    path('contest/<contest_id>/duel/', views.duel, name="duel"),
]
