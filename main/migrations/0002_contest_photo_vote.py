# Generated by Django 2.0.3 on 2018-03-31 01:39

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import main.models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contest',
            fields=[
                ('id', models.CharField(default=main.models.generate_uuid, editable=False, max_length=30, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=200)),
                ('description', models.TextField(blank=True)),
                ('participation', models.CharField(choices=[('PU', 'Open'), ('PR', 'Private')], default='PU', max_length=200)),
                ('voting', models.CharField(choices=[('PU', 'Open'), ('PR', 'Private')], default='PU', max_length=200)),
                ('visibility', models.CharField(choices=[('PU', 'Open'), ('PR', 'Private')], default='PU', max_length=200)),
                ('creation_timestamp', models.DateTimeField(auto_now_add=True)),
                ('creator', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.CharField(default=main.models.generate_uuid, editable=False, max_length=30, primary_key=True, serialize=False)),
                ('url', models.CharField(max_length=2000)),
                ('creation_timestamp', models.DateTimeField(auto_now_add=True)),
                ('creator', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Vote',
            fields=[
                ('id', models.CharField(default=main.models.generate_uuid, editable=False, max_length=30, primary_key=True, serialize=False)),
                ('contest', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Contest')),
                ('photo', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Photo')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
