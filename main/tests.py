import os

from django.conf import settings
from django.template import Template
from django.test import TestCase


class SmokeTests(TestCase):
    def test_compile_templates(self):
        for template_dir in settings.TEMPLATES[0]["DIRS"]:
            for basepath, dirs, filenames in os.walk(template_dir):
                for filename in filenames:
                    path = os.path.join(basepath, filename)
                    with open(path, "r") as f:
                        # This will fail if the template cannot compile
                        t = Template(f.read())
                        assert t
