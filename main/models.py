import bleach
import mistune
import shortuuid
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _


def render_markdown(text) -> str:
    """Render Markdown, disallowing some tags and cleaning it up afterwards."""
    allowed_tags = [
        'a', 'abbr', 'acronym', 'b', 'blockquote', 'code', 'em', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'i', 'li', 'ol',
        'p', 'pre', 'span', 'strong', 'ul'
    ]
    allowed_attributes = {
        'a': ['href', 'title'],
        'acronym': ['title'],
        'abbr': ['title'],
        'span': ['data-expounder', 'data-expounded']
    }

    rendered = mistune.markdown(text, escape=False)
    cleaned = bleach.clean(rendered, tags=allowed_tags, attributes=allowed_attributes)
    linkified = bleach.linkify(cleaned)
    return linkified


def generate_username() -> str:
    """Create a UUID."""
    return shortuuid.ShortUUID().random(8)


def generate_uuid() -> str:
    """Create a UUID."""
    return shortuuid.ShortUUID().random(8)


class CharIDModel(models.Model):
    """Base model that gives children string IDs."""

    id = models.CharField(max_length=30, primary_key=True, default=generate_uuid, editable=False)

    class Meta:
        abstract = True


class User(AbstractUser):
    username = models.CharField(
        _('username'),
        max_length=150,
        default=generate_username,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[UnicodeUsernameValidator()],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )


class Contest(CharIDModel):
    PUBLIC = "PU"
    PRIVATE = "PR"

    OPENNESS = [
        (PUBLIC, _("Open")),
        (PRIVATE, _("Private")),
    ]

    SUBMISSION = "SUB"
    VOTING = "VOT"
    COMPLETE = "COM"

    STAGE = [
        (SUBMISSION, _("Submission")),
        (VOTING, _("Voting")),
        (COMPLETE, _("Complete")),
    ]

    name = models.CharField(max_length=200)
    description = models.TextField(blank=True)
    creator = models.ForeignKey(User, on_delete=models.CASCADE)

    participation = models.CharField(max_length=200, choices=OPENNESS, default=PUBLIC)
    voting = models.CharField(max_length=200, choices=OPENNESS, default=PUBLIC)
    visibility = models.CharField(max_length=200, choices=OPENNESS, default=PUBLIC)

    stage = models.CharField(max_length=200, choices=STAGE, default=SUBMISSION)

    creation_timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name

    def get_rendered_description(self) -> str:
        return render_markdown(self.description)

    def get_absolute_url(self):
        return reverse('main:contest', args=[self.id])


class Photo(CharIDModel):
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    contest = models.ForeignKey(Contest, on_delete=models.CASCADE)
    url = models.CharField(max_length=2000)
    creation_timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.id


class Vote(CharIDModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    contest = models.ForeignKey(Contest, on_delete=models.CASCADE)
    winner = models.ForeignKey(Photo, on_delete=models.CASCADE, related_name="winners")
    loser = models.ForeignKey(Photo, on_delete=models.CASCADE, related_name="losers")

    def __str__(self):
        return "%s - %s > %s" % (self.contest, self.winner, self.loser)
