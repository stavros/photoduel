from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect, render
from django.utils.translation import ugettext as _
from django.views.decorators.http import require_http_methods

from .models import Contest, Vote


@require_http_methods(["GET", "POST"])
@login_required
def duel(request, contest_id):
    contest = get_object_or_404(Contest, pk=contest_id)
    if not contest.photo_set.exists():
        messages.error(request, _("That contest doesn't have any photos yet."))
        return redirect(contest)

    if contest.stage != Contest.VOTING:
        if contest.stage == Contest.SUBMISSION:
            messages.error(request, _("Voting on that contest hasn't opened yet."))
        else:
            messages.error(request, _("Voting on that contest has ended."))
        return redirect(contest)

    if request.method == "POST":
        winner = contest.photo_set.filter(pk=request.POST.get("winner_id")).first()
        loser = contest.photo_set.filter(pk=request.POST.get("loser_id")).first()

        if not (winner and loser):
            messages.error(request, _("Invalid photo selected."))
            return redirect("main:duel", contest_id)

        Vote.objects.create(
            user=request.user,
            contest=contest,
            winner=winner,
            loser=loser,
        )
        return redirect("main:duel", contest_id)
    else:
        photo1 = contest.photo_set.all().order_by("?").first()
        photo2 = contest.photo_set.exclude(pk=photo1.id).order_by("?").first()
        return render(request, "duel.html", locals())


def contest(request, contest_id):
    contest = get_object_or_404(Contest, pk=contest_id)
    return render(request, "contest.html", locals())


def index(request):
    return render(request, "index.html")


def login(request):
    return render(request, "login.html")
