terraform {
  backend "s3" {
    bucket = "terraformstate.stochastic.io"
    key    = "photoduel/terraform.tfstate"
    region = "eu-central-1"
  }
}

variable "cloudflare_email" {}
variable "cloudflare_token" {}

variable ipv6_ip { default = "2a01:4f8:1c0c:6109::1" }
variable ipv4_ip { default = "195.201.40.251" }
variable domain { default = "photoduel.net" }

provider "cloudflare" {
  email = "${var.cloudflare_email}"
  token = "${var.cloudflare_token}"
}

resource "cloudflare_record" "v6_root" {
  domain="${var.domain}"
  type="AAAA"
  name="@"
  proxied="true"
  value="${var.ipv6_ip}"
}

resource "cloudflare_record" "v6_www" {
  name="www"
  domain="${var.domain}"
  type="AAAA"
  proxied="true"
  value="${var.ipv6_ip}"
}

resource "cloudflare_record" "root" {
  name="@"
  domain="${var.domain}"
  type="A"
  proxied="true"
  value="${var.ipv4_ip}"
}

resource "cloudflare_record" "www" {
  name="www"
  domain="${var.domain}"
  type="A"
  proxied="true"
  value="${var.ipv4_ip}"
}

resource "cloudflare_record" "email" {
  name="email"
  domain="${var.domain}"
  type="CNAME"
  value="u6640409.wl160.sendgrid.net"
}

resource "cloudflare_record" "s1_domainkey" {
  domain="${var.domain}"
  type="CNAME"
  name="s1._domainkey"
  value="s1.domainkey.u6640409.wl160.sendgrid.net"
}

resource "cloudflare_record" "s2_domainkey" {
  domain="${var.domain}"
  type="CNAME"
  name="s2._domainkey"
  value="s2.domainkey.u6640409.wl160.sendgrid.net"
}

resource "cloudflare_record" "mx10" {
  domain="${var.domain}"
  type="MX"
  name="@"
  priority="10"
  value="mxa.mailgun.com"
}

resource "cloudflare_record" "mx20" {
  domain="${var.domain}"
  type="MX"
  name="@"
  priority="20"
  value="mxb.mailgun.com"
}
